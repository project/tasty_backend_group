<?php

namespace Drupal\tasty_backend_group;

use Drupal\group\Entity\GroupType;
use Drupal\views\Views;
use Drupal\group\Entity\GroupContentType;

/**
 * Tasty Backend Group Manager Service.
 */
class TastyBackendGroupManager {

  /**
   * Add a new administration view for a group type.
   *
   * @param \Drupal\group\Entity\GroupType $type
   *   Group Type object.
   */
  public static function addAdminView(GroupType $type) {

    // Default view doesn't have any type set.
    $type_filter = [
      'id' => 'type',
      'table' => 'groups_field_data',
      'field' => 'type',
      'group_type' => 'group',
      'value' => [
        $type->id() => $type->id(),
      ],
      'entity_type' => 'group',
      'entity_field' => 'type',
      'plugin_id' => 'bundle',
      'group' => 1,
    ];

    // Duplicate the view.
    $view = Views::getView('tb_group_manage_groups')->storage->createDuplicate();

    // Set some basic info.
    $view->setStatus(TRUE);
    $view->set('id', 'tb_group_manage_groups_' . $type->id());
    $view->set('label', 'Tasty Backend Group Manage ' . $type->label());
    $view->set('description', 'Tasty Backend Group administration view to manage all ' . $type->label() . ' groups.');

    // Set the display options.
    $display = $view->get('display');
    // @todo Add a new permission to access the view or use group type permissions.
    // Group module doesn't seem to provide global edit or update permissions
    // outside of the advanced outsider permissions.
    $display['default']['display_options']['access']['options']['perm'] = 'create ' . $type->id() . ' group';
    $display['default']['display_options']['filters']['type'] = $type_filter;
    $display['default']['display_options']['title'] = 'Manage ' . $type->label() . ' groups';
    $display['page_1']['display_options']['path'] = 'admin/manage/group/groups/' . $type->id();
    $display['page_1']['display_options']['menu']['title'] = $type->label();
    $display['page_1']['display_options']['menu']['description'] = 'Manage ' . $type->label() . ' groups.';
    $view->set('display', $display);

    // Save the new view.
    $view->save();

    // Set watchdog message that the new view was created.
    \Drupal::logger('tasty_backend_group')->notice('Created new Tasty Backend Group management view: @view.', ['@view' => $view->label()]);
  }

  /**
   * Add default permissions for a group content type.
   *
   * @param Drupal\group\Entity\GroupContentType $type
   *   Group content type.
   * @param string $rid
   *   The ID of a user role to alter.
   */
  public static function addGroupContentTypePermissions(GroupContentType $type, $rid = NULL) {
    $role = '';

    // Allow this to be run if you know the role ID, otherwise we'll
    // load the role by properties.
    if ($rid) {
      $role = \Drupal::entityTypeManager()->getStorage('group_role')->load($rid);
    }
    else {
      // We need to load the role by properties due the way Group
      // creates the IDs for the roles. I can't seem to find the
      // original tb_group_admin ID in there anywhere.
      $role_properties = [
        'label' => 'Group admin',
        'audience' => 'outsider',
      ];
      /** @var \Drupal\group\Entity\GroupRoleInterface $role */
      $role = \Drupal::entityTypeManager()->getStorage('group_role')->loadByProperties($role_properties);
      $role = reset($role);
    }

    if ($role) {
      $content_plugin_id = $type->getContentPluginId();
      $permissions = [
        "create $content_plugin_id entity",
        "delete any $content_plugin_id entity",
        "update any $content_plugin_id entity",
        "view $content_plugin_id entity",
        "view unpublished $content_plugin_id entity",
        "create $content_plugin_id content",
        "delete any $content_plugin_id content",
        "update any $content_plugin_id content",
        "view $content_plugin_id content",
      ];
      $role->grantPermissions($permissions)->trustData()->save();

      $args = [
        '%role_name' => $role->label(),
        '%type' => $type->label(),
      ];
      \Drupal::messenger()->addMessage(t('Default group content type permissions have been added to the %role_name role for the %type group content type.', $args));
    }
  }

}
